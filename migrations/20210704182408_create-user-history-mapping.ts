import {Knex} from "knex";

export async function up(knex:Knex):Promise<void>{
	await knex.schema.createTable("user_history_mapping",(table) => {
		table.increments();
		table.integer("user_id").unsigned();
		table.foreign("user_id").references("users.id");
		table.integer("history_id");
		table.foreign("history_id").references("history.id");
	})
}

export async function down(knex:Knex):Promise<void>{
	await knex.schema.dropTable("user_history_mapping");
}
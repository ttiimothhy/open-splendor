import {Knex} from "knex";

export async function up(knex:Knex):Promise<void>{
	await knex.schema.createTable("user_room_mapping",(table) => {
		table.increments();
		table.integer("user_id").unsigned();
		table.foreign("user_id").references("users.id");
		table.integer("room_id").unsigned();
		table.foreign("room_id").references("rooms.id");
	})
}

export async function down(knex:Knex):Promise<void>{
	await knex.schema.dropTable("user_room_mapping");
}
import {Knex} from "knex";

export async function up(knex:Knex):Promise<void>{
	await knex.schema.createTable("history",(table) => {
		table.increments();
		table.string("uuid");
		table.string("winner");
		table.jsonb("result");
		table.integer("time_spent");
		table.integer("num_of_player");
		table.string("video_name");
		table.integer("num_of_term");
		table.timestamp("finish_time")
	})
}

export async function down(knex:Knex):Promise<void>{
	await knex.schema.dropTable("history");
}
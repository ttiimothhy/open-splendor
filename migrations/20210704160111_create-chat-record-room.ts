import {Knex} from "knex";

export async function up(knex:Knex):Promise<void>{
	await knex.schema.createTable("chat_record_room",(table) => {
		table.increments();
		table.string("name");
		table.jsonb("message");
		table.string("room");
		table.timestamp("date_send");
	})
}

export async function down(knex:Knex):Promise<void>{
	await knex.schema.dropTable("chat_record_room");
}
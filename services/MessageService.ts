import {Client} from "pg";

export class MessageService{
	constructor(private client:Client){}

	async transmission(transmission:string,uuId:string){
		await this.client.query(/*sql*/`update rooms set transmission = $1 where uuid = $2`,[transmission,uuId]);
	}

	async guestMessage(name:string,message:string){
		await this.client.query(/*sql*/`insert into chat_record(name,message,date_send)
		values ($1,$2,now())`,[name,{message}]);
	}

	async userMessage(name:string,message:string){
		await this.client.query(/*sql*/`insert into chat_record_user(name,message,date_send)
		values ($1,$2,now())`,[name,{message}]);
	}

	async roomMessage(uuId:string,name:string,message:string){
		const roomId = (await this.client.query(/*sql*/`select id from rooms where uuid = $1`,
		[uuId])).rows[0].id;
		await this.client.query(/*sql*/`insert into chat_record_room(name,message,room,date_send)
		values ($1,$2,$3,now())`,[name,{message},roomId]);
	}

	async loadChatGlobal(){
		return (await this.client.query(/*sql*/`select * from chat_record`)).rows;
	}

	async loadChatUser(){
		return (await this.client.query(/*sql*/`select * from chat_record_user`)).rows;
	}

	async loadChatRoom(uuId:string){
		const roomId = (await this.client.query(/*sql*/`select id from rooms where uuid = $1`,
		[uuId])).rows[0].id;
		return (await this.client.query(/*sql*/`select * from chat_record_room where room = $1`,
		[roomId])).rows;
	}

	async loadChatFriend(name:string){
		return (await this.client.query(/*sql*/`select * from chat_record_friend where belong_to = $1`,
		[name])).rows;
	}

	async storeChatFriend(name:string,message:string,belongUser:string){
		await this.client.query(/*sql*/`insert into chat_record_friend(name,message,belong_to,date_send) values
		($1,$2,$3,now())`,[name,{message},belongUser]);
	}

	async storeVoiceFriend(name:string,message:string,belongUser:string){
		await this.client.query(/*sql*/`insert into chat_record_friend(name,message,belong_to,date_send) values
		($1,$2,$3,now())`,[name,{message},belongUser]);
	}
}
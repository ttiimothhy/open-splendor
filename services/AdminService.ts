import {Client} from "pg";

export class AdminService{
	constructor(private client:Client){}

	async addQuestion(name:string,question:string){
		return (await this.client.query(/*sql*/`insert into question_bank(name,question,answer) values ($1,$2,'')`,
		[name,question]));
	}

	async loadAdminQuestion(){
		return (await this.client.query(/*sql*/`select * from question_bank order by id`)).rows;
	}

	async loadUserQuestion(name:string){
		return (await this.client.query(/*sql*/`select * from question_bank where name = $1 order by id`,
		[name])).rows;
	}

	async answerQuestion(content:string,question:string){
		await this.client.query(/*sql*/`update question_bank set answer = $1 where question = $2`,
		[`answer: ${content}`,question]);
	}
}
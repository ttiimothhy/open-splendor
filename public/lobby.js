export let currentUser;
let socket;
let messagePanel1 = document.querySelector(".message-area-1");
let messagePanel2 = document.querySelector(".message-area-2");
let messagePanel3 = document.querySelector(".message-area-3");
let messagePanel4 = document.querySelector(".message-area-4");
let roomGlobal = document.querySelector(".global");
let roomUser = document.querySelector('.user');
let roomRoom = document.querySelector(".room");
let roomPrivate = document.querySelector('.private');
let messageForm1 = document.querySelector(".message-form-1");
let messageForm2 = document.querySelector(".message-form-2");
let messageForm3 = document.querySelector(".message-form-3");
let messageForm4 = document.querySelector(".message-form-4");
let userprofile = document.querySelector(".username");
let email = document.querySelector(".email");
let icon = document.querySelector(".icon");
let preference = document.querySelector(".preference-button");
let cross = document.querySelector(".imageflex");
let secondCross = document.querySelector(".second-imageflex");
let addFriendSwitch = [];
let removeFriendSwitch = [];
let secondFullDisplay = document.querySelector(".second-full-display");
let thirdFullDisplay = document.querySelector(".third-full-display");
let loadFriend = document.querySelector(".friend-button");
const bgmSoundGain = document.querySelector("#bgm-volume");
const effectSoundGain = document.querySelector("#effect-volume");
let audioContext = new AudioContext({ latencyHint: 0, sampleRate: 44100 }); //,sampleRate:20000}
let effectGainNode = new GainNode(audioContext,{gain:effectSoundGain.value})
let bgmGainNode = new GainNode(audioContext, { gain: bgmSoundGain.value });
let startBgm = document.querySelector("#start-bgm");
let changeSong = document.querySelector(".background-song");
let soundSource;
let newSong = changeSong.value.split(' ').join('');
let loadFriendList = document.querySelector('.load-friend');

// Samuel :
// now lobby.html using both javascript.js & lobby.js will call io.connect twice so serve will run io.connection twice

// let socket;
function initSocket() {
    socket = io.connect();
    socket.on("global-chatroom",function(data){
        let li = document.createElement("li")
        li.innerText = data;
        messagePanel1.appendChild(li);

    })
    socket.on("user-chatroom",function(data){
        let li = document.createElement("li")
        li.innerText = data;
        messagePanel2.appendChild(li);
    })
    socket.on("room-chatroom",function(data){
        let li = document.createElement("li")
        li.innerText = data;
        messagePanel3.appendChild(li);
    })
    socket.on("friend-chatroom",async function(data){
        let li = document.createElement("li")
        li.innerText = data.message;
        messagePanel4.appendChild(li);

        let message = {};
        message.name = data.name;
        message.content = data.content;
        message.belong = userprofile.innerText;
        await fetch("/store-chat-friend",{
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(message)
        })
    })
}
initSocket();

$(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
})
const createRoomButton = document.querySelector(".create-room-button");
createRoomButton.addEventListener("mouseover",async()=>{
    await getSoundLobby("startGameButton","effect")
})
createRoomButton.addEventListener("click",async()=>{
    const res = await fetch("/create-room")
    const resJSON = await res.json();
	// console.log(resJSON)
    if(resJSON.success === false){
        alert("You are already in a room!");
    }else{
        window.location.href = `/room/${resJSON}`
    }
})

function roomGenerator(roomDataJSON){
    document.querySelector(".gameroom-join").innerHTML = "";
    const roomData = JSON.parse(roomDataJSON);

    //remove duplicated room based on uuid
    const filteredRoomData = roomData.reduce((previous, current) => {
        if (!previous[0]) {
            previous.push(current)
            return previous
        }
        const duplicatedRoom = previous.find(item => item.uuid === current.uuid)
        if (!duplicatedRoom) {
            previous.push(current)
        }
        return previous;
    },[])
	// console.log(filteredRoomData)
    filteredRoomData.forEach((room)=>{
        const roomId = room.id;
        const uuId = room.uuid;
        const isStarted = room.is_started;
        const numOfPlayers = room.num_of_players;
        const name = room.username;
        if(roomId > 0){
            document.querySelector(".gameroom-join").innerHTML += `<div class="room-information">
                <div>
                    Room ${roomId} Player : ${numOfPlayers}/4
                </div>
                <button class="join-button" ${isStarted?"disabled":""}}>
                <a href="/room/${uuId}">${isStarted?"Started":"Join"}</a>
                </button>
                <div class="player-label">
                    <div class="individual-player">
                        <i class="fas fa-universal-access player-picture"></i>
                        <div class="player-name">
                            <div class="no-player-name">${name}</div>
                            <div class="add-friend-area">
                            </div>
                        </div>
                    </div>
                </div>
            </div>`
            let joinRoomButton = document.querySelectorAll('.join-button');
            for(let everyJoinButton of joinRoomButton){
                everyJoinButton.addEventListener("mouseover",async()=>{
                    await getSoundLobby("startGameButton","effect");
                })
            }
            let addFriend = document.querySelectorAll(".player-name");
            let addFriendArea = document.querySelectorAll(".add-friend-area");
            let friendName = document.querySelectorAll(".no-player-name");
            for(let i = 0; i < addFriend.length; i++){
                addFriendSwitch.push(true);
            }

            for (let i = 0; i < addFriend.length; i++) {
                addFriend[i].addEventListener("click", function(e){
                    if (addFriendSwitch[i]){
                        let addFriendContent = document.querySelectorAll(".add-friend-content");
                        if(addFriendContent.length === 0){
                            addFriendArea[i].innerHTML = `<div class="add-friend-content">
                                <i class="fas fa-chevron-up"></i>
                                <div class="word add-friend-word">Add friend</div>
                                <div class="word status" value="${e.currentTarget.innerText}">Status</div>
                            </div>`;

                            //samuel add for player leaderbroad
                            document.querySelector(".status").addEventListener("click",(e)=>{
                                const playerStatus = e.currentTarget.getAttribute("value")
                                document.querySelector(".main-area").innerHTML = `<iframe width="100%" height="100%" src="/history.html?${playerStatus}" title="Player History"></iframe>`
                            })
                            let addFriendWord = document.querySelector(".add-friend-word");
                            addFriendWord.addEventListener('click',async function(){
                                if(friendName[i]){
                                    if (!(friendName[i].innerText.includes('Guest'))) {
                                        await fetch('/add-friend', {
                                            method: "POST",
                                            headers: {
                                                "Content-Type": "application/json"
                                            },
                                            body: JSON.stringify({name:friendName[i].innerText})
                                        });

                                        socket.disconnect();
                                        initSocket();
                                    }
                                }
                            })
                            addFriendSwitch[i] = false;
                        }
                    }else{
                        addFriendArea[i].innerHTML = '';
                        addFriendSwitch[i] = true;
                    }
                })
            }
        }
    })
}
socket.on("room-information",(roomData)=>{
    roomGenerator(roomData);
})

function userStatusChecking(data) {
    const isPlaying = data.isPlaying;
    const isInRoom = data.isInRoom;
    const uuId = data.uuId;
    if(isPlaying && !isInRoom){
        document.querySelector(".load-game-button").style.display = "block"
        document.querySelector(".create-room-button").disabled = true
    }
    document.querySelector(".load-game-button").addEventListener("click", () => {
        window.location.href = `/room/${uuId}`
    })
}
socket.on("user-game-information",(userGameData)=>{
    userStatusChecking(userGameData);
})

roomGlobal.addEventListener("click", function () {
    messagePanel1.classList.add("chatroom-active");
    messagePanel2.classList.remove("chatroom-active");
    messagePanel3.classList.remove("chatroom-active");
    messagePanel4.classList.remove("chatroom-active");
    messageForm1.classList.add("message-active");
    messageForm2.classList.remove("message-active");
    messageForm3.classList.remove("message-active");
    messageForm4.classList.remove("message-active");
    roomGlobal.classList.add("title-active");
    roomUser.classList.remove("title-active");
    roomRoom.classList.remove("title-active");
    roomPrivate.classList.remove("title-active");
})
roomUser.addEventListener("click", async function () {
    messagePanel1.classList.remove("chatroom-active");
    messagePanel2.classList.add("chatroom-active");
    messagePanel3.classList.remove("chatroom-active");
    messagePanel4.classList.remove("chatroom-active");
    messageForm1.classList.remove("message-active");
    messageForm2.classList.add("message-active");
    messageForm3.classList.remove("message-active");
    messageForm4.classList.remove("message-active");
    roomGlobal.classList.remove("title-active");
    roomUser.classList.add("title-active");
    roomRoom.classList.remove("title-active");
    roomPrivate.classList.remove("title-active");
})
roomRoom.addEventListener("click",function(){
    messagePanel1.classList.remove("chatroom-active");
    messagePanel2.classList.remove("chatroom-active");
    messagePanel3.classList.add("chatroom-active");
    messagePanel4.classList.remove("chatroom-active");
    messageForm1.classList.remove("message-active");
    messageForm2.classList.remove("message-active");
    messageForm3.classList.add("message-active");
    messageForm4.classList.remove("message-active");
    roomGlobal.classList.remove("title-active");
    roomUser.classList.remove("title-active");
    roomRoom.classList.add("title-active");
    roomPrivate.classList.remove("title-active");
})
roomPrivate.addEventListener("click",function(){
    messagePanel1.classList.remove("chatroom-active");
    messagePanel2.classList.remove("chatroom-active");
    messagePanel3.classList.remove("chatroom-active");
    messagePanel4.classList.add("chatroom-active");
    messageForm1.classList.remove("message-active");
    messageForm2.classList.remove("message-active");
    messageForm3.classList.remove("message-active");
    messageForm4.classList.add("message-active");
    roomGlobal.classList.remove("title-active");
    roomUser.classList.remove("title-active");
    roomRoom.classList.remove("title-active");
    roomPrivate.classList.add("title-active");
})

preference.addEventListener("click",function(){
    secondFullDisplay.style = "display:block";
})
cross.addEventListener("click",function(){
    secondFullDisplay.style = "display:none";
})

loadFriend.addEventListener("click",function(){
    thirdFullDisplay.style = "display:block";
})
secondCross.addEventListener("click",function(){
    thirdFullDisplay.style = "display:none";
})

document.querySelector('.message-form-1').addEventListener('submit',async(event)=>{
    // 截咗原本個submit
    event.preventDefault();
    // 攞返form嘅element出嚟
    const form = event.currentTarget;

    // create your form object with the form inputs
    await fetch('/message/1',{
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ content: form.content.value })
    });
    form.reset();
})

document.querySelector('.message-form-2').addEventListener('submit',async(event)=>{
    // 截咗原本個submit
    event.preventDefault();
    // 攞返form嘅element出嚟
    const form = event.currentTarget;

    // create your form object with the form inputs
    if(currentUser){
        await fetch('/message/2',{
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({content:form.content.value})
        });
    }
    form.reset();
})

document.querySelector('.message-form-3').addEventListener('submit',async(event)=>{
    // 截咗原本個submit
    event.preventDefault();
    // 攞返form嘅element出嚟
    const form = event.currentTarget;

    // create your form object with the form inputs
    await fetch('/message/3',{
        method: "POST",
        headers:{
            "Content-Type": "application/json"
        },
        body: JSON.stringify({content:form.content.value})
    });
    form.reset();
})

document.querySelector('.message-form-4').addEventListener('submit',async(event)=>{
    // 截咗原本個submit
    event.preventDefault();
    // 攞返form嘅element出嚟
    const form = event.currentTarget;

    // create your form object with the form inputs
    await fetch('/message/4',{
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({content:form.content.value})
    });
    form.reset();
})

async function chatRecordGlobal(){
    const res = await fetch('/chat-record-global');
    const result = await res.json();
    if(messagePanel1){
        for(let message of result){
            if(message.message.message){
                if(!(message.message.message[0].type === "Buffer")) {
                    let li = document.createElement("li")
                    li.innerText = `${message.name}: ${message.message.message}`;
                    messagePanel1.appendChild(li);
                }else{
                    let soundBuffer = new Uint8Array(message.message.message[0].data).buffer;
                    let arrayBuffer = [soundBuffer];
                    let audioData = new Blob(arrayBuffer,{'type':'audio/mp3'});
                    let li = document.createElement('li');

                    // Creating audio url with reference
                    // of created blob named 'audioData'
                    let audioSrc = window.URL.createObjectURL(audioData);

                    // Pass the audio url to the 2nd video tag
                    li.innerHTML = `${message.name}: <audio src="${audioSrc}" controls></audio>`
                    messagePanel1.appendChild(li);
                }
            }else{
                let li = document.createElement("li")
                li.innerText = `${message.name}: ${message.message.message}`;
                messagePanel1.appendChild(li);
            }
        }
    }
}

async function chatRecordUser(){
    const res = await fetch('/chat-record-user');
    const result = await res.json();
    if(messageForm2){
        for(let message of result){
            if(message.message.message){
                if(!(message.message.message[0].type === "Buffer")){
                    let li = document.createElement("li")
                    li.innerText = `${message.name}: ${message.message.message}`;
                    messagePanel2.appendChild(li);
                }else{
                    let soundBuffer = new Uint8Array(message.message.message[0].data).buffer;
                    let arrayBuffer = [soundBuffer];
                    let audioData = new Blob(arrayBuffer,{'type':'audio/mp3'});
                    let li = document.createElement('li');
                    // Creating audio url with reference
                    // of created blob named 'audioData'
                    let audioSrc = window.URL.createObjectURL(audioData);
                    // Pass the audio url to the 2nd video tag
                    li.innerHTML = `${message.name}: <audio src="${audioSrc}" controls></audio>`
                    messagePanel2.appendChild(li);
                }
            }else{
                let li = document.createElement("li")
                li.innerText = `${message.name}: ${message.message.message}`;
                messagePanel2.appendChild(li);
            }
        }
    }
}

async function chatRecordRoom(){
    const res = await fetch('/chat-record-room');
    const result = await res.json();
    if(messageForm3){
        if(result.length){
            for(let message of result){
                if(message.message.message){
                    if(!(message.message.message[0].type === "Buffer")){
                        let li = document.createElement("li")
                        li.innerText = `${message.name}: ${message.message.message}`;
                        messagePanel3.appendChild(li);
                    }else{
                        let soundBuffer = new Uint8Array(message.message.message[0].data).buffer;
                        let arrayBuffer = [soundBuffer];
                        let audioData = new Blob(arrayBuffer,{'type':'audio/mp3'});
                        let li = document.createElement('li');
                        // Creating audio url with reference
                        // of created blob named 'audioData'
                        let audioSrc = window.URL.createObjectURL(audioData);
                        // Pass the audio url to the 2nd video tag
                        li.innerHTML = `${message.name}: <audio src="${audioSrc}" controls></audio>`
                        messagePanel3.appendChild(li);
                    }
                }else{
                    let li = document.createElement("li")
                    li.innerText = `${message.name}: ${message.message.message}`;
                    messagePanel3.appendChild(li);
                }
            }
        }
    }
}

async function chatRecordFriend(){
    const res = await fetch('/chat-record-friend');
    const result = await res.json();

    if(messageForm4){
        for(let message of result){
            if(message.message.message){
                if (!(message.message.message.includes('/uploads/'))){
                    let li = document.createElement("li");
                    li.innerText = `${message.name}: ${message.message.message}`;
                    messagePanel4.appendChild(li);
                }else{
                    let li = document.createElement('li');
                    li.innerHTML = `${message.name}: <audio src="${message.message.message}" controls></audio>`
                    messagePanel4.appendChild(li);
                }
            }else{
                let li = document.createElement("li");
                    li.innerText = `${message.name}: ${message.message.message}`;
                    messagePanel4.appendChild(li);
            }
        }
    }
}

async function guestGenerate(){
    if(!currentUser){
        await fetch('/guest');
        await getCurrentUser();
        socket.disconnect();
        initSocket();
    }
}

async function getCurrentUser(){
    const res = await fetch('/current-user');
    const result = await res.json();
    if(result.success){
        currentUser = result.user;
        userprofile.innerText = currentUser.name;
        if(currentUser.email){
            let oldText = `@${currentUser.email}`;
            let newText;
            if(oldText.length > 24){
                newText = oldText.substring(0, 24) + '\n' + oldText.substring(24, oldText.length);
            }else{
                newText = oldText;
            }
            email.innerText = newText;
        }else{
            email.innerText = `@${currentUser.name}`;
        }
        if(currentUser.icon){
            icon.innerHTML = `<img src="${currentUser.icon}" id="icon-picture">`;
        }else{
            icon.innerHTML = '<i class="fas fa-street-view"></i>';
        }
    }
}
async function loadFriendNow(){
    let res = await fetch('/load-friend');
    let friendList = await res.json();

    loadFriendList.innerHTML = '';
    for(let onefriend of friendList){
        if(onefriend.name.length > 10){
            onefriend.name = onefriend.name.substring(0, 10) + ' ' + onefriend.name.substring(10, onefriend.name.length);
        }
        loadFriendList.innerHTML += `<li class="one-friend">
            <div class="one-ranking-name">${onefriend.name}</div>
            <div class="one-ranking-number"></div>
            <div class="one-online-time"></div>
            <div class="one-remove-friend">
            </div>
        </li>`;
    }
}
loadFriend.addEventListener('click',async function(){
    await loadFriendNow();
    let everyFriendArea = document.querySelectorAll(".one-remove-friend");
    let everyFriendName = document.querySelectorAll(".one-ranking-name");
    for(let i = 0; i < everyFriendArea.length; i++){
        removeFriendSwitch.push(true);
        everyFriendName[i].addEventListener("click",function(){

            if (removeFriendSwitch[i]){
                let removeFriendDecision = document.querySelectorAll('.remove-friend');
                if (removeFriendDecision.length === 0){
                    everyFriendArea[i].innerHTML = `<div class="remove-friend">
                        <div class="remove-friend-word">Remove friend</div>
                        <i class="fas fa-chevron-left"></i>
                    </div>`
                    removeFriendSwitch[i] = false;
                    let removeFriendButton = document.querySelector(".remove-friend-word");
                    removeFriendButton.addEventListener("click",async function() {
                        await fetch('/remove-friend',{
                            method:"POST",
                            headers:{
                                "Content-Type": "application/json"
                            },
                            body: JSON.stringify({name:everyFriendName[i].innerText})
                        })
                        everyFriendArea[i].innerHTML = "";
                        await loadFriendNow();
                        socket.disconnect();
                        initSocket();
                        removeFriendSwitch[i] = true;
                    })
                }
            }else{
                everyFriendArea[i].innerHTML = "";
                removeFriendSwitch[i] = true;
            }
        })
    }
})

async function recording(){
    let audioIN = {audio:true};
    const mediaStreamObj = await navigator.mediaDevices.getUserMedia(audioIN);

    let startGlobal = document.querySelector("#microphone-1");
    let startRecodingGlobal = document.querySelector(".start-recording-1");
    let microphoneGlobalClick = true;
    let mediaRecorderGlobal = new MediaRecorder(mediaStreamObj);

    let startUser = document.querySelector("#microphone-2");
    let startRecodingUser = document.querySelector(".start-recording-2");
    let microphoneUserClick = true;
    let mediaRecorderUser = new MediaRecorder(mediaStreamObj);

    let startRoom = document.querySelector("#microphone-3");
    let startRecodingRoom = document.querySelector(".start-recording-3");
    let microphoneRoomClick = true;
    let mediaRecorderRoom = new MediaRecorder(mediaStreamObj);

    let startFriend = document.querySelector("#microphone-4");
    let startRecodingFriend = document.querySelector(".start-recording-4");
    let microphoneFriendClick = true;
    let mediaRecorderFriend = new MediaRecorder(mediaStreamObj);

    startGlobal.addEventListener('click',function(){
        if(microphoneGlobalClick){
            let changeColor = true
            mediaRecorderGlobal.start();
            microphoneGlobalClick = false;
            startGlobal.classList.add("microphone-active");
            startRecodingGlobal.innerHTML = '<i class="far fa-comment-dots"></i>';
            setInterval(function () {
                if (changeColor) {
                    startRecodingGlobal.style = "color:#ffffff";
                    changeColor = false;
                } else {
                    startRecodingGlobal.style = "color:#1d1f20";
                    changeColor = true;
                }
            },800)
        }else{
            mediaRecorderGlobal.stop();
            microphoneGlobalClick = true;
            startGlobal.classList.remove("microphone-active");
            startRecodingGlobal.innerHTML = '';
        }
    })

    startUser.addEventListener('click',function(){
        if (microphoneUserClick) {
            let changeColor = true
            mediaRecorderUser.start();
            microphoneUserClick = false;
            startUser.classList.add("microphone-active");
            startRecodingUser.innerHTML = '<i class="far fa-comment-dots"></i>';
            setInterval(function(){
                if(changeColor){
                    startRecodingUser.style = "color:#ffffff";
                    changeColor = false;
                } else {
                    startRecodingUser.style = "color:#1d1f20";
                    changeColor = true;
                }
            },800)
        }else{
            mediaRecorderUser.stop();
            microphoneUserClick = true;
            startUser.classList.remove("microphone-active");
            startRecodingUser.innerHTML = '';
        }
    })

    startRoom.addEventListener('click',function(){
        if(microphoneRoomClick){
            let changeColor = true
            mediaRecorderRoom.start();
            microphoneRoomClick = false;
            startRoom.classList.add("microphone-active");
            startRecodingRoom.innerHTML = '<i class="far fa-comment-dots"></i>';
            setInterval(function(){
                if(changeColor){
                    startRecodingRoom.style = "color:#ffffff";
                    changeColor = false;
                }else{
                    startRecodingRoom.style = "color:#1d1f20";
                    changeColor = true;
                }
            },800)
        }else{
            mediaRecorderRoom.stop();
            microphoneRoomClick = true;
            startRoom.classList.remove("microphone-active");
            startRecodingRoom.innerHTML = '';
        }
    })

    startFriend.addEventListener('click',function(){
        if(microphoneFriendClick){
            let changeColor = true
            mediaRecorderFriend.start();
            microphoneFriendClick = false;
            startFriend.classList.add("microphone-active");
            startRecodingFriend.innerHTML = '<i class="far fa-comment-dots"></i>';
            setInterval(function(){
                if(changeColor){
                    startRecodingFriend.style = "color:#ffffff";
                    changeColor = false;
                }else{
                    startRecodingFriend.style = "color:#1d1f20";
                    changeColor = true;
                }
            },800)
        }else{
            mediaRecorderFriend.stop();
            microphoneFriendClick = true;
            startFriend.classList.remove("microphone-active");
            startRecodingFriend.innerHTML = '';
        }
    })

    let dataArray = [];
    mediaRecorderGlobal.ondataavailable = function(event){
        dataArray.push(event.data);
    }
    // Convert the audio data in to blob
    // after stopping the recording
    mediaRecorderGlobal.onstop = async function(){
        // blob of type mp3
        socket.emit("sound-global",dataArray);
        // After fill up the chunk
        // array make it empty
        dataArray = [];
    }
    mediaRecorderUser.ondataavailable = function(event){
        dataArray.push(event.data);
    }
    mediaRecorderUser.onstop = async function(){
        // blob of type mp3
        socket.emit("sound-user", dataArray);
        // After fill up the chunk
        // array make it empty
        dataArray = [];
    }

    mediaRecorderRoom.ondataavailable = function(event){
        dataArray.push(event.data);
    }
    mediaRecorderRoom.onstop = async function(){
        // blob of type mp3

        socket.emit("sound-room", dataArray);
        // After fill up the chunk
        // array make it empty
        dataArray = [];
    }

    mediaRecorderFriend.ondataavailable = function(event){
        dataArray.push(event.data);
    }
    mediaRecorderFriend.onstop = async function(){
        // blob of type mp3
        socket.emit("sound-friend",dataArray);
        // After fill up the chunk
        // array make it empty
        dataArray = [];
    }
    socket.on("sound-transverse-global",(soundData)=>{
        let audioData = new Blob(soundData.message, {'type':'audio/mp3'});
        let li = document.createElement('li');

        // Creating audio url with reference
        // of created blob named 'audioData'
        let audioSrc = window.URL.createObjectURL(audioData);

        // Pass the audio url to the 2nd video tag
        li.innerHTML = `${soundData.user}: <audio src="${audioSrc}" controls></audio>`
        messagePanel1.appendChild(li);
    })

    socket.on("sound-transverse-user",(soundData)=>{

        let audioData = new Blob(soundData.message, {'type':'audio/mp3'});
        let li = document.createElement('li');

        // Creating audio url with reference
        // of created blob named 'audioData'
        let audioSrc = window.URL.createObjectURL(audioData);

        // Pass the audio url to the 2nd video tag
        li.innerHTML = `${soundData.user}: <audio src="${audioSrc}" controls></audio>`
        messagePanel2.appendChild(li);
    })

    socket.on("sound-transverse-room",(soundData)=>{

        let audioData = new Blob(soundData.message,{'type':'audio/mp3'});
        let li = document.createElement('li');

        // Creating audio url with reference
        // of created blob named 'audioData'
        let audioSrc = window.URL.createObjectURL(audioData);

        // Pass the audio url to the 2nd video tag
        li.innerHTML = `${soundData.user}: <audio src="${audioSrc}" controls></audio>`
        messagePanel3.appendChild(li);
    })

    socket.on("sound-transverse-friend",async(soundData)=>{
        let audioData = new Blob(soundData.message,{'type':'audio/mp3'});
        let li = document.createElement('li');

        // Creating audio url with reference
        // of created blob named 'audioData'
        let audioSrc = window.URL.createObjectURL(audioData);

        // Pass the audio url to the 2nd video tag
        li.innerHTML = `${soundData.user}: <audio src="${audioSrc}" controls></audio>`
        messagePanel4.appendChild(li);

        const message = new FormData();

        const blob = new Blob(soundData.message);
        const sound = new File([blob],'filename.mp3');

        message.append('name', soundData.user);
        message.append('content',sound);
        message.append('belong',userprofile.innerText);
        await fetch("/store-voice-friend",{
            method: "POST",
            body:message
        })
    })
}

function setupEvenetListeners(){
    effectSoundGain.addEventListener("input",(event)=>{
        const value = parseFloat(event.target.value);
        effectGainNode.gain.setValueAtTime(value, audioContext.currentTime);
    })
    bgmSoundGain.addEventListener("input",(event)=>{
        const value = parseFloat(event.target.value);
        bgmGainNode.gain.setValueAtTime(value, audioContext.currentTime);
    })
}
async function getSoundLobby(sound,use){

    const res = await fetch(`/lobby-sound?name=${sound}`);
    const soundBuffer = await res.arrayBuffer();

    const getTokenBuffer = await audioContext.decodeAudioData(soundBuffer);
    soundSource = audioContext.createBufferSource();
    soundSource.buffer = getTokenBuffer;
    if(use === "bgm"){
        // var loopingEnabled = soundSource.loop;
        soundSource.loop = true ;
        soundSource.connect(bgmGainNode)
        bgmGainNode.connect(audioContext.destination);
    }else if(use === "effect"){
        soundSource.connect(effectGainNode);
        effectGainNode.connect(audioContext.destination);
    }
    soundSource.start();
}
setupEvenetListeners();
let startBgmSwitch = 0;

startBgm.addEventListener('click',async function(){
    if(startBgmSwitch === 0){
        getSoundLobby(newSong,"bgm");
        startBgmSwitch = -1;
    }else if(startBgmSwitch === -1){
        bgmGainNode.disconnect(audioContext.destination);
        startBgmSwitch = 1;

    }else if(startBgmSwitch === 1){
        bgmGainNode.connect(audioContext.destination);
        startBgmSwitch = -1;
    }
})

changeSong.addEventListener("input",async function(){
    if(soundSource){
        soundSource.stop();
    }
    if(changeSong.value){
        newSong = changeSong.value.split(' ').join('');
        setupEvenetListeners();
        await getSoundLobby(newSong,"bgm");
    }
})


window.onload = async function(){
    await getCurrentUser();
    await guestGenerate();
    await chatRecordGlobal();
    await chatRecordUser();
    await chatRecordRoom();
    await chatRecordFriend();
    await recording();
}
document.querySelector(".history").value = currentUser //history leaderbroad get users name
document.querySelector(".top-left-title").addEventListener("click",()=>{
    window.location.href = "/lobby.html"
})
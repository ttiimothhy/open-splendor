import {currentUser} from "./enquiry.js"

document.querySelector(".history").addEventListener("click", () => {
    const name = currentUser.name
    document.querySelector(".main-area").innerHTML = `
    <iframe  width="100%" height="100%" src="/history.html?${name}" title="Player History"></iframe>
    `
})


document.querySelector(".rank").addEventListener("click", () => {
    document.querySelector(".main-area").innerHTML = `
    <iframe  width="100%" height="100%" src="/ranking.html" title="Player Rank"></iframe>
    `
})

document.querySelector(".player-search").addEventListener("click", () => {
    const name = document.querySelector(".player-name").value
    console.log(name)
    document.querySelector(".main-area").innerHTML = `
    <iframe  width="100%" height="100%" src="/history.html?${name}" title="Player History"></iframe>
    `
})
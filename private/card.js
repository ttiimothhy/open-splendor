class Card {
    constructor(tier,id, score, production, cost) {
        this.tier = tier
        this.id = id
        this.score = score
        this.production = production
        this.cost = cost
    }
}
class Card1 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 1
        this.score = 1
        this.production = "white"
        this.cost = {green:4}
    }
}
class Card2 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 2
        this.score = 0
        this.production = "blue"
        this.cost = {red:1,green:3,blue:1}
    }
}
class Card3 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 3
        this.score = 1
        this.production = "green"
        this.cost = {brown:4}
    }
}
class Card4 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 4
        this.score = 0
        this.production = "brown"
        this.cost = {red:3,green:1,brown:1}
    }
}
class Card5 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 5
        this.score = 0
        this.production = "blue"
        this.cost = {brown:2,white:1}
    }
}
class Card6 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 6
        this.score = 0
        this.production = "red"
        this.cost = {red:2,white:2}
    }
}
class Card7 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 7
        this.score = 0
        this.production = "brown"
        this.cost = {green:2,white:2}
    }
}
class Card8 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 8
        this.score = 0
        this.production = "blue"
        this.cost = {brown:3}
    }
}

class Card9 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 9
        this.score = 1
        this.production = "brown"
        this.cost = {blue:4}
    }
}

class Card10 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 10
        this.score = 0
        this.production = "brown"
        this.cost = {blue:2,brown:2}
    }
}
class Card11 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 11
        this.score = 0
        this.production = "green"
        this.cost = {red:1,blue:1,brown:2,white:1}
    }
}
class Card12 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 12
        this.score = 0
        this.production = "green"
        this.cost = {red:1,blue:1,brown:1,white:1}
    }
}
class Card13 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 13
        this.score = 0
        this.production = "white"
        this.cost = {blue:3}
    }
}
class Card14 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 14
        this.score = 0
        this.production = "white"
        this.cost = {red:1,green:1,blue:1,brown:1}
    }
}
class Card15 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 15
        this.score = 0
        this.production = "brown"
        this.cost = {red:1,blue:2,white:2}
    }
}
class Card16 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 16
        this.score = 0
        this.production = "brown"
        this.cost = {red:1,green:1,blue:2,white:1}
    }
}
class Card17 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 17
        this.score = 0
        this.production = "brown"
        this.cost = {red:1,green:2}
    }
}
class Card18 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 18
        this.score = 0
        this.production = "red"
        this.cost = {green:1,blue:2}
    }
}
class Card19 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 19
        this.score = 0
        this.production = "red"
        this.cost = {red:1,brown:3,white:1}
    }
}
class Card20 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 20
        this.score = 0
        this.production = "red"
        this.cost = {green:1,blue:1,brown:1,white:2}
    }
}
class Card21 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 21
        this.score = 0
        this.production = "blue"
        this.cost = {red:1,green:1,brown:1,white:1}
    }
}
class Card22 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 22
        this.score = 1
        this.production = "blue"
        this.cost = {red:4}
    }
}
class Card23 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 23
        this.score = 0
        this.production = "blue"
        this.cost = {red:2,green:2,white:1}
    }
}
class Card24 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 24
        this.score = 0
        this.production = "green"
        this.cost = {red:3}
    }
}
class Card25 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 25
        this.score = 0
        this.production = "green"
        this.cost = {red:2,blue:1,brown:2}
    }
}
class Card26 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 26
        this.score = 0
        this.production = "green"
        this.cost = {green:1,blue:3,white:1}
    }
}
class Card27 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 27
        this.score = 0
        this.production = "green"
        this.cost = {blue:1,white:2}
    }
}
class Card28 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 28
        this.score = 1
        this.production = "red"
        this.cost = {white:4}
    }
}
class Card29 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 29
        this.score = 0
        this.production = "red"
        this.cost = {green:1,brown:2,white:2}
    }
}
class Card30 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 30
        this.score = 0
        this.production = "red"
        this.cost = {green:1,blue:1,brown:1,white:1}
    }
}
class Card31 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 31
        this.score = 0
        this.production = "white"
        this.cost = {green:2,blue:2,brown:1}
    }
}
class Card32 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 32
        this.score = 0
        this.production = "white"
        this.cost = {blue:1,brown:1,white:3}
    }
}
class Card33 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 33
        this.score = 0
        this.production = "white"
        this.cost = {red:2,brown:1}
    }
}
class Card34 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 34
        this.score = 0
        this.production = "blue"
        this.cost = {red:2,green:1,brown:1,white:1}
    }
}
class Card35 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 35
        this.score = 0
        this.production = "blue"
        this.cost = {red:2,green:1,brown:1,white:1}
    }
}
class Card36 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 36
        this.score = 0
        this.production = "blue"
        this.cost = {green:2,brown:2}
    }
}
class Card37 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 37
        this.score = 0
        this.production = "brown"
        this.cost = {red:1,green:1,blue:1,white:1}
    }
}
class Card38 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 38
        this.score = 0
        this.production = "green"
        this.cost = {red:2,blue:2}
    }
}
class Card39 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 39
        this.score = 0
        this.production = "brown"
        this.cost = {green:3}
    }
}
class Card40 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "LL"
        this.id = 40
        this.score = 0
        this.production = "red"
        this.cost = {white:3}
    }
}

class Card41 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 41
        this.score = 1
        this.production = "green"
        this.cost = {green:2,red:3,white:3}
    }
}
class Card42 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 42
        this.score = 2
        this.production = "green"
        this.cost = {green:5}
    }
}
class Card43 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 43
        this.score = 2
        this.production = "green"
        this.cost = {green:3,blue:5}
    }
}
class Card44 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 44
        this.score = 3
        this.production = "white"
        this.cost = {white:6}
    }
}
class Card45 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 45
        this.score = 2
        this.production = "blue"
        this.cost = {blue:5}
    }
}
class Card46 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 46
        this.score = 1
        this.production = "green"
        this.cost = {blue:3,brown:2,white:2}
    }
}
class Card47 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 47
        this.score = 2
        this.production = "red"
        this.cost = {green:2,blue:4,white:1}
    }
}
class Card48 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 48
        this.score = 3
        this.production = "red"
        this.cost = {red:6}
    }
}
class Card49 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 49
        this.score = 2
        this.production = "brown"
        this.cost = {green:5,red:3}
    }
}
class Card50 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 50
        this.score = 1
        this.production = "brown"
        this.cost = {green:2,white:3,blue:2}
    }
}
class Card51 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 51
        this.score = 2
        this.production = "white"
        this.cost = {green:1,red:4,brown:2}
    }
}
class Card52 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 52
        this.score = 1
        this.production = "blue"
        this.cost = {green:3,brown:3,blue:2}
    }
}
class Card53 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 53
        this.score = 3
        this.production = "green"
        this.cost = {green:6}
    }
}
class Card54 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 54
        this.score = 1
        this.production = "white"
        this.cost = {blue:3,white:2,red:3}
    }
}
class Card55 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 55
        this.score = 1
        this.production = "brown"
        this.cost = {green:3,white:3,brown:2}
    }
}
class Card56 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 56
        this.score = 2
        this.production = "white"
        this.cost = {red:3}
    }
}
class Card57 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 57
        this.score = 2
        this.production = "red"
        this.cost = {brown:5}
    }
}
class Card58 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 58
        this.score = 3
        this.production = "brown"
        this.cost = {brown:6}
    }
}
class Card59 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 59
        this.score = 1
        this.production = "red"
        this.cost = {red:2,white:2,brown:3}
    }
}
class Card60 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 60
        this.score = 2
        this.production = "red"
        this.cost = {white:3,brown:5}
    }
}
class Card61 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 61
        this.score = 2
        this.production = "brown"
        this.cost = {green:4,red:2,blue:1}
    }
}
class Card62 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 62
        this.score = 3
        this.production = "blue"
        this.cost = {blue:6}
    }
}
class Card63 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 63
        this.score = 2
        this.production = "brown"
        this.cost = {white:5}
    }
}
class Card64 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 64
        this.score = 2
        this.production = "green"
        this.cost = {white:4,brown:1,blue:2}
    }
}
class Card65 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 65
        this.score = 2
        this.production = "blue"
        this.cost = {white:2,red:1,brown:4}
    }
}
class Card66 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 66
        this.score = 1
        this.production = "white"
        this.cost = {green:3,red:2,brown:2}
    }
}
class Card67 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 67
        this.score = 1
        this.production = "blue"
        this.cost = {green:2,red:3,blue:2}
    }
}
class Card68 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 68
        this.score = 1
        this.production = "red"
        this.cost = {blue:3,brown:3,red:2}
    }
}
class Card69 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 69
        this.score = 2
        this.production = "blue"
        this.cost = {white:5,blue:3}
    }
}
class Card70 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "ML"
        this.id = 70
        this.score = 2
        this.production = "white"
        this.cost = {brown:3,red:5}
    }
}
class Card71 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "HL"
        this.id = 71
        this.score = 4
        this.production = "white"
        this.cost = {brown:6,white:3,red:3}
    }
}
class Card72 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "HL"
        this.id = 72
        this.score = 4
        this.production = "green"
        this.cost = {white:3,blue:6,green:3}
    }
}
class Card73 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "HL"
        this.id = 73
        this.score = 3
        this.production = "blue"
        this.cost = {brown:5,white:3,red:3,green:3}
    }
}
class Card74 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "HL"
        this.id = 74
        this.score = 4
        this.production = "white"
        this.cost = {brown:7}
    }
}
class Card75 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "HL"
        this.id = 75
        this.score = 5
        this.production = "brown"
        this.cost = {brown:3,red:7}
    }
}
class Card76 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "HL"
        this.id = 76
        this.score = 5
        this.production = "blue"
        this.cost = {blue:3,white:7}
    }
}
class Card77 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "HL"
        this.id = 77
        this.score = 5
        this.production = "red"
        this.cost = {green:7,red:3}
    }
}
class Card78 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "HL"
        this.id = 78
        this.score = 3
        this.production = "brown"
        this.cost = {green:5,blue:3,white:3,red:3}
    }
}
class Card79 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "HL"
        this.id = 79
        this.score = 4
        this.production = "red"
        this.cost = {green:7}
    }
}
class Card80 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "HL"
        this.id = 80
        this.score = 4
        this.production = "blue"
        this.cost = {white:6,blue:3,brown:3}
    }
}
class Card81 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "HL"
        this.id = 70
        this.score = 4
        this.production = "green"
        this.cost = {blue:7}
    }
}
class Card82 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "HL"
        this.id = 82
        this.score = 5
        this.production = "green"
        this.cost = {blue:7,green:3}
    }
}
class Card83 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "HL"
        this.id = 83
        this.score = 3
        this.production = "green"
        this.cost = {white:5,blue:3,red:3,brown:3}
    }
}
class Card84 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "HL"
        this.id = 84
        this.score = 4
        this.production = "brown"
        this.cost = {brown:3,green:3,red:6}
    }
}
class Card85 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "HL"
        this.id = 85
        this.score = 3
        this.production = "red"
        this.cost = {brown:3,white:3,green:3,blue:5}
    }
}
class Card86 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "HL"
        this.id = 86
        this.score = 5 
        this.production = "white"
        this.cost = {brown:7,white:3}
    }
}
class Card87 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "HL"
        this.id = 87
        this.score = 4
        this.production = "red"
        this.cost = {blue:3,green:6,red:3}
    }
}
class Card88 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "HL"
        this.id = 88
        this.score = 4
        this.production = "blue"
        this.cost = {white:7}
    }
}
class Card89 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "HL"
        this.id = 89
        this.score = 3
        this.production = "white"
        this.cost = {red:5,green:3,blue:3,brown:3}
    }
}
class Card90 extends Card {
    constructor(tier,id, score, production, cost) {
        super(tier,id, score, production, cost)
        this.tier = "HL"
        this.id = 90
        this.score = 4
        this.production = "brown"
        this.cost = {red:7}
    }
}
export const lowCardArr=[new Card1,new Card2,new Card3,new Card4,new Card5,new Card6,new Card7,
    new Card8,new Card9,new Card10,new Card11,new Card12,new Card13,new Card14,new Card15,new Card16,
    new Card17,new Card18,new Card19,new Card20,new Card21,new Card22,new Card23,new Card24,new Card25,
    new Card26,new Card27,new Card28,new Card29,new Card30,new Card31,new Card32,new Card33,new Card34,
    new Card35,new Card36,new Card37,new Card38,new Card39,new Card40]

export const midCardArr=[new Card41,new Card42,new Card43,new Card44,new Card45,new Card46,new Card47,
    new Card48,new Card49,new Card50,new Card51,new Card52,new Card53,new Card54,new Card55,new Card56,
    new Card57,new Card58,new Card59,new Card60,new Card61,new Card62,new Card63,new Card64,new Card65,
    new Card66,new Card67,new Card68,new Card69,new Card70]
export const highCardArr=[new Card71,new Card72,new Card73,new Card74,new Card75,new Card76,new Card77,
    new Card78,new Card79,new Card80,new Card81,new Card82,new Card83,new Card84,new Card85,new Card86,
    new Card87,new Card88,new Card89,new Card90]

class NovelCard {
    constructor(id, score, productionRequirement) {
        this.id = id
        this.score = score
        this.productionRequirement = productionRequirement
    }
}

class NovelCard1 extends NovelCard{
    constructor(id, score, productionRequirement) {
        super(id, score, productionRequirement)
        this.id = 1
        this.score = 3
        this.productionRequirement = {brown:3,red:3,white:3}
    }
}
class NovelCard2 extends NovelCard{
    constructor(id, score, productionRequirement) {
        super(id, score, productionRequirement)
        this.id = 2
        this.score = 3
        this.productionRequirement = {brown:3,red:3,green:3}
    }
}
class NovelCard3 extends NovelCard{
    constructor(id, score, productionRequirement) {
        super(id, score, productionRequirement)
        this.id = 3
        this.score = 3
        this.productionRequirement = {red:4,green:4}
    }
}
class NovelCard4 extends NovelCard{
    constructor(id, score, productionRequirement) {
        super(id, score, productionRequirement)
        this.id = 4
        this.score = 3
        this.productionRequirement = {green:3,blue:3,white:3}
    }
}
class NovelCard5 extends NovelCard{
    constructor(id, score, productionRequirement) {
        super(id, score, productionRequirement)
        this.id = 5
        this.score = 3
        this.productionRequirement = {blue:4,green:4}
    }
}
class NovelCard6 extends NovelCard{
    constructor(id, score, productionRequirement) {
        super(id, score, productionRequirement)
        this.id = 6
        this.score = 3
        this.productionRequirement = {greeen:3,blue:3,red:3}
    }
}
class NovelCard7 extends NovelCard{
    constructor(id, score, productionRequirement) {
        super(id, score, productionRequirement)
        this.id = 7
        this.score = 3
        this.productionRequirement = {brown:3,blue:3,white:3}
    }
}
class NovelCard8 extends NovelCard{
    constructor(id, score, productionRequirement) {
        super(id, score, productionRequirement)
        this.id = 8
        this.score = 3
        this.productionRequirement = {blue:4,white:4}
    }
}
class NovelCard9 extends NovelCard{
    constructor(id, score, productionRequirement) {
        super(id, score, productionRequirement)
        this.id = 9
        this.score = 3
        this.productionRequirement = {brown:4,white:4}
    }
}
class NovelCard10 extends NovelCard{
    constructor(id, score, productionRequirement) {
        super(id, score, productionRequirement)
        this.id = 10
        this.score = 3
        this.productionRequirement = {brown:4,red:4}
    }
}

class test1 extends NovelCard{
    constructor(id, score, productionRequirement) {
        super(id, score, productionRequirement)
        this.id = 1
        this.score = 3
        this.productionRequirement = {blue:1}
    }
}
class test2 extends NovelCard{
    constructor(id, score, productionRequirement) {
        super(id, score, productionRequirement)
        this.id = 2
        this.score = 3
        this.productionRequirement = {blue:1}
    }
}

class test3 extends NovelCard{
    constructor(id, score, productionRequirement) {
        super(id, score, productionRequirement)
        this.id = 3
        this.score = 3
        this.productionRequirement = {green:1}
    }
}
class test4 extends NovelCard{
    constructor(id, score, productionRequirement) {
        super(id, score, productionRequirement)
        this.id = 4
        this.score = 3
        this.productionRequirement = {brown:1}
    }
}
class test5 extends NovelCard{
    constructor(id, score, productionRequirement) {
        super(id, score, productionRequirement)
        this.id = 5
        this.score = 3
        this.productionRequirement = {red:1}
    }
}

// export const novelCardDeck= [new NovelCard1,new NovelCard2,new NovelCard3,new NovelCard4,new NovelCard5,
//     new NovelCard6,new NovelCard7,new NovelCard8,new NovelCard9,new NovelCard10]

export const novelCardDeck =[new test1,new test2,new test3,new test4,new test5]